package com.codingdojo.dataprovider;
import static org.junit.Assert.*;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;

import com.codingdojo.dataprovider.FragebogenParser;
import com.codingdojo.daten.Frage;

public class FragebogenParserTest {

	@Test
	public void testDeserialisiere() {

		String fragebogenText = "?Frage" + System.lineSeparator() + "a" + System.lineSeparator() + "*b"
				+ System.lineSeparator() + "c" + System.lineSeparator() + "?Frage2" + System.lineSeparator() + "d"
				+ System.lineSeparator() + "e" + System.lineSeparator() + "*f" + System.lineSeparator() + "g"
				+ System.lineSeparator();
		List<Frage> frageBogen = FragebogenParser.deserialisiere(fragebogenText);

		assertEquals("Frage?", frageBogen.get(0).getFragetext());
		assertEquals("a", frageBogen.get(0).getAntwortmoeglichkeiten().get(0).getAntwort());
		assertEquals("b", frageBogen.get(0).getAntwortmoeglichkeiten().get(1).getAntwort());
		assertEquals("c", frageBogen.get(0).getAntwortmoeglichkeiten().get(2).getAntwort());
		assertTrue(frageBogen.get(0).getAntwortmoeglichkeiten().get(1).istRichtig());
		assertFalse(frageBogen.get(0).getAntwortmoeglichkeiten().get(0).istRichtig());
		assertFalse(frageBogen.get(0).getAntwortmoeglichkeiten().get(2).istRichtig());

		assertEquals("Frage2?", frageBogen.get(1).getFragetext());
		assertEquals("d", frageBogen.get(1).getAntwortmoeglichkeiten().get(0).getAntwort());
		assertEquals("e", frageBogen.get(1).getAntwortmoeglichkeiten().get(1).getAntwort());
		assertEquals("f", frageBogen.get(1).getAntwortmoeglichkeiten().get(2).getAntwort());
		assertEquals("g", frageBogen.get(1).getAntwortmoeglichkeiten().get(3).getAntwort());
		assertTrue(frageBogen.get(1).getAntwortmoeglichkeiten().get(2).istRichtig());
		assertFalse(frageBogen.get(1).getAntwortmoeglichkeiten().get(0).istRichtig());
		assertFalse(frageBogen.get(1).getAntwortmoeglichkeiten().get(1).istRichtig());
		assertFalse(frageBogen.get(1).getAntwortmoeglichkeiten().get(3).istRichtig());
	}

	
}

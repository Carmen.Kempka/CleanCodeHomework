package com.codingdojo.daten;

public class AuswertungsMock extends Auswertung {
	
	public AuswertungsMock(int anzahlFragen, int anzahlKorrekteAntworten) {
		super(anzahlFragen);
		this.korrekteAntworten = anzahlKorrekteAntworten;
	}
	
}

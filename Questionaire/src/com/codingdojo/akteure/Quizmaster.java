package com.codingdojo.akteure;

import java.io.IOException;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map.Entry;
import java.util.Set;

import com.codingdojo.daten.Aufgabe;
import com.codingdojo.daten.Auswertung;
import com.codingdojo.daten.Frage;
import com.codingdojo.ui.Aufgabendialog;
import com.codingdojo.ui.AuswertungsDialog;

public class Quizmaster {

	protected LinkedHashMap<Frage,Set<Integer>> beantworteteFragen;

	public Quizmaster() {
		beantworteteFragen = new LinkedHashMap<>();
	}

	public void quizDurchfuehren(List<Frage> fragebogen) throws IOException{
		stelleAufgaben(fragebogen);
		auswertungAnzeigen();
	}
	
	
	private void stelleAufgaben(List<Frage> fragebogen) throws IOException {
		Aufgabendialog aufgabendialog = new Aufgabendialog();
		int frageNummer = 1;
		for (Frage aktuelleFrage : fragebogen) {
			Aufgabe aufgabe = new Aufgabe(
					aktuelleFrage.getFragetext(), 
					aktuelleFrage.getAntwortmoeglichkeiten(), 
					frageNummer, 
					fragebogen.size());
			stelleAufgabe(aufgabendialog, aktuelleFrage, aufgabe);
			frageNummer++;
		}
	}

	private void stelleAufgabe(Aufgabendialog aufgabendialog, Frage aktuelleFrage, Aufgabe aufgabe) throws IOException {
		aufgabendialog.zeige(aufgabe);
		Set<Integer> antwortIndizes = aufgabendialog.leseAntwort();
		this.merke(aktuelleFrage, antwortIndizes);
	}


	private void auswertungAnzeigen() {
		Auswertung auswertung = auswerten();
		AuswertungsDialog dialog = new AuswertungsDialog();
		dialog.zeige(auswertung);
	}
	
	
	private void merke(Frage aktuelleFrage, Set<Integer> antwortIndizes) {
		beantworteteFragen.put(aktuelleFrage, antwortIndizes);
	}
	

	protected Auswertung auswerten() {
		Auswertung auswertung = new Auswertung(beantworteteFragen.size());
		
		for ( Entry<Frage, Set<Integer>> antwort : beantworteteFragen.entrySet()) {
			Frage beantworteteFrage = antwort.getKey();
			
			if (beantworteteFrage.istFrageRichtigBeantwortet(antwort.getValue())) {
				auswertung.addKorrekteAntwort();
			} else {
				auswertung.addKorrektur(antwort.getKey(), antwort.getValue());
			}
		}
		return auswertung;
	}


}

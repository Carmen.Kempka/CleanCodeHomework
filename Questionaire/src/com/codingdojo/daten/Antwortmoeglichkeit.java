package com.codingdojo.daten;

public class Antwortmoeglichkeit {

	private String antwort;
	private boolean istRichtig;

	public Antwortmoeglichkeit(String antwort, boolean istRichtig) {
		this.antwort = antwort;
		this.istRichtig = istRichtig;
	}

	public String getAntwort() {
		return antwort;
	}

	public boolean istRichtig() {
		return istRichtig;
	}

}

package com.codingdojo.daten;

import java.util.List;
import java.util.Set;

public class Korrektur {

	private List<String> gegebeneAntworten;
	private String frageText;
	private List<String> richtigeAntworten;
	
	public Korrektur(Frage frage,  Set<Integer> antwort){
		this.frageText = frage.getFragetext();
		this.richtigeAntworten = frage.getRichtigeAntwortenText();
		this.gegebeneAntworten = frage.generiereTextuelleAntwort(antwort);
	}

	public String getFrageText() {
		return frageText;
	}

	public List<String> getRichtigeAntworten() {
		return richtigeAntworten;
	}
	
	public List<String> getGegebeneAntworten() {
		return gegebeneAntworten;
	}

}

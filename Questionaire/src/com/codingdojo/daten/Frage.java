package com.codingdojo.daten;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

public class Frage {

	private String fragetext;
	private List<Antwortmoeglichkeit> antwortmoeglichkeiten;

	public Frage(String fragetext) {
		this.fragetext = fragetext;
		antwortmoeglichkeiten = new ArrayList<>();
	}

	public String getFragetext() {
		return fragetext;
	}

	public List<Antwortmoeglichkeit> getAntwortmoeglichkeiten() {
		return antwortmoeglichkeiten;
	}
	
	public void addAntwortmoeglichkeit(Antwortmoeglichkeit antwortmoeglichkeit){
		antwortmoeglichkeiten.add(antwortmoeglichkeit);
	}
	
	public Set<Integer> getRichtigeAntwortIndizes() {
		TreeSet<Integer> antwortIndizes = new TreeSet<>();
		
		for (int i=0; i<antwortmoeglichkeiten.size(); i++) {
			if (antwortmoeglichkeiten.get(i).istRichtig()) {
				antwortIndizes.add(i);
			}
		}
		return antwortIndizes;
	}
	
	public List<String> getRichtigeAntwortenText(){
		 List<String> richtigeAntworten = new ArrayList<>();
		 for (Integer i : getRichtigeAntwortIndizes()) {
			 richtigeAntworten.add(antwortmoeglichkeiten.get(i).getAntwort());
		}
		return richtigeAntworten;
	}
	
	public List<String> generiereTextuelleAntwort(Set<Integer> gegebeneAntwort) {
		List<String> gegebeneAntwortenAlsText = new ArrayList<>();
		for (Integer i : gegebeneAntwort) {
			if(i < antwortmoeglichkeiten.size()){
				gegebeneAntwortenAlsText.add(antwortmoeglichkeiten.get(i).getAntwort());
			} else{
				gegebeneAntwortenAlsText.add("keine Ahnung");
			}
		}
		return gegebeneAntwortenAlsText;
	}
	
	public boolean istFrageRichtigBeantwortet(Set<Integer> antworten) {
		Set<Integer> richtigeAntwortIndizes = this.getRichtigeAntwortIndizes();
		
		return antworten.containsAll(richtigeAntwortIndizes) && 
				(antworten.size() == richtigeAntwortIndizes.size());
		
	}

}

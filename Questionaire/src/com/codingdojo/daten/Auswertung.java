package com.codingdojo.daten;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.Map.Entry;

public class Auswertung {

	private int anzahlFragen;

	int korrekteAntworten;
	private List<Korrektur> korrekturen;

	public Auswertung(int anzahlFragen) {
		this.anzahlFragen = anzahlFragen;
		this.korrekteAntworten = 0;
		this.korrekturen = new ArrayList<>();
	}
	
	public void addKorrektur(Frage frage, Set<Integer> gegebeneAntworten){
		korrekturen.add(new Korrektur(frage, gegebeneAntworten));
	}

	

	public int berechneProzent() {
		return korrekteAntworten * 100 / anzahlFragen;
	}
	
	public int getAnzahlFragen() {
		return anzahlFragen;
	}

	public int getKorrekteAntworten() {
		return korrekteAntworten;
	}

	public void addKorrekteAntwort() {
		this.korrekteAntworten += 1;
	}

	public List<Korrektur> getKorrekturen() {
		return korrekturen;
	}

	

}

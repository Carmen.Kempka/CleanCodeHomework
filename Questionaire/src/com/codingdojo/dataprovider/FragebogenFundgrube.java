package com.codingdojo.dataprovider;

import java.io.IOException;
import java.util.List;

import com.codingdojo.dataprovider.fileio.FileIO;
import com.codingdojo.daten.Frage;

public class FragebogenFundgrube {
	
	public List<Frage> holeFragebogen() throws IOException {
		FileIO fileIO = new FileIO();

		String fragetext = fileIO.fragebogenLesen();
		return FragebogenParser.deserialisiere(fragetext);
	}
}

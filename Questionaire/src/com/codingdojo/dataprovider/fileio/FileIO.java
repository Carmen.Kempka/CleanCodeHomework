package com.codingdojo.dataprovider.fileio;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

public class FileIO {

	public String fragebogenLesen() throws IOException{
		File file =new File("questionaire.txt");
		return new String(Files.readAllBytes(file.toPath()));
	}
}

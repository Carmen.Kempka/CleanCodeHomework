package com.codingdojo.ui;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.List;
import java.util.Scanner;
import java.util.Set;
import java.util.TreeSet;

import com.codingdojo.daten.Antwortmoeglichkeit;
import com.codingdojo.daten.Aufgabe;

public class Aufgabendialog {
	Scanner antwortScanner;

	public Aufgabendialog() {
		antwortScanner = new Scanner(System.in);
	}

	public void zeige(Aufgabe aufgabe) throws IOException {
		String dialogtext = erstelleDialogtext(aufgabe);
		gibDialogtextAus(dialogtext);
	}

	protected String erstelleDialogtext(Aufgabe aufgabe) {
		String kopfzeilenText = formuliereKopfzeile(aufgabe);
		String aufgabenText = formuliereAufgabe(aufgabe);
		return kopfzeilenText + aufgabenText;
	}

	public Set<Integer> leseAntwort() throws IOException {
		String antworten = antwortScanner.nextLine();
		return parseAntworten(antworten);
	}

	private TreeSet<Integer> parseAntworten(String antworten) {
		TreeSet<Integer> antwortIndizes = new TreeSet<>();
		
		String[] antwortArray = antworten.split(",");
		for (String antwort : antwortArray) {
			antwortIndizes.add(Integer.parseInt(antwort.trim())-1);
		}
		return antwortIndizes;
	}

	private String formuliereKopfzeile(Aufgabe aufgabe) {
		return MessageFormat.format("Frage {0} von {1}{2}", aufgabe.getAufgabenIndex(), aufgabe.getAnzahlAufgaben(),
				System.lineSeparator());
	}

	private String formuliereAufgabe(Aufgabe aufgabe) {
		StringBuilder aufgabentextStringBuilder = new StringBuilder();
		aufgabentextStringBuilder.append(aufgabe.getFrageText());
		List<Antwortmoeglichkeit> antwortMoeglichkeiten = aufgabe.getAntwortMoeglichkeiten();

		for (int i = 0; i < antwortMoeglichkeiten.size(); i++) {
			aufgabentextStringBuilder.append(System.lineSeparator());
			int antwortNummer = i + 1;
			String antwortZeile = MessageFormat.format("{0}) {1}", antwortNummer,
					antwortMoeglichkeiten.get(i).getAntwort());
			aufgabentextStringBuilder.append(antwortZeile);
		}
		String dontKnow = MessageFormat.format("{0}) Keine Ahnung", antwortMoeglichkeiten.size() + 1);
		aufgabentextStringBuilder.append(System.lineSeparator());
		aufgabentextStringBuilder.append(dontKnow);
		return aufgabentextStringBuilder.toString();
	}

	private void gibDialogtextAus(String dialogtext) {
		System.out.println(dialogtext);
	}

	protected void finalize() throws Throwable {
		antwortScanner.close();
	}

}

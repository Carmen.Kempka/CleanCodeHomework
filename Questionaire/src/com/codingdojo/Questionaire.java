package com.codingdojo;

import java.io.IOException;

import com.codingdojo.akteure.Regie;
import com.codingdojo.daten.Aufgabe;
import com.codingdojo.ui.Aufgabendialog;

public class Questionaire {

	public static void main(String[] args) throws IOException {
		Regie regie = new Regie();
		
		regie.quizStarten();
	}
}
